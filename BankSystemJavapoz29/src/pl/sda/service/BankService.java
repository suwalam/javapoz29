package pl.sda.service;

import pl.sda.dao.ClientDao;
import pl.sda.dao.ClientDaoFile;
import pl.sda.exception.IndexValidationException;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.exception.InvalidAccountNumberException;
import pl.sda.exception.NegativeAmountException;
import pl.sda.model.Account;
import pl.sda.model.Client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class BankService {

    private List<Client> clients;

    private ClientDao clientDAO;

    public BankService() throws IOException {
        clientDAO = new ClientDaoFile();
        clients = clientDAO.getAll();
    }

    public List<Client> get() {
        return clients;
    }

    public void save() throws IOException {
        clientDAO.saveAll(clients);
    }

    public void payment(List<Account> accounts, double amount, int index) throws IndexValidationException, NegativeAmountException {

        index = index - 1; //klient liczy od 1, a my od 0 więc zmniejszamy index

        validateIndex(accounts.size(), index);

        accounts.get(index).payment(BigDecimal.valueOf(amount));

    }

    public void withdrawal(List<Account> accounts, double amount, int index) throws IndexValidationException, InsufficientBalanceException, NegativeAmountException {

        index = index -1;

        validateIndex(accounts.size(), index);

        accounts.get(index).withdrawal(BigDecimal.valueOf(amount));

    }

    public void transfer(List<Account> accounts, double amount, int index, String number) throws InvalidAccountNumberException, IndexValidationException, InsufficientBalanceException, NegativeAmountException {

        index = index -1;

        validateIndex(accounts.size(), index);

        Account destinationAccount = getAccountByNumber(number);

        accounts.get(index).transfer(destinationAccount, BigDecimal.valueOf(amount));




    }

    private Account getAccountByNumber(String number) throws InvalidAccountNumberException {

        return clients.stream()
                .map(c -> c.getAccounts())
                .flatMap(a -> a.stream())
                .filter(a -> a.getNumber().equals(number))
                .findFirst()
                .orElseThrow(() -> new InvalidAccountNumberException("Podany numer konta jest błędny"));
    }

    private void validateIndex(int size, int index) throws IndexValidationException {
        if (index < 0 || index >= size) {
            throw new IndexValidationException("Podano nieprawidłowy indeks.");
        }
    }


}