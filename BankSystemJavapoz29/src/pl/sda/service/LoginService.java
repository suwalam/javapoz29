package pl.sda.service;

import pl.sda.model.Client;

import java.util.List;

public class LoginService {

    private List<Client> clients;

    public LoginService(List<Client> clients) {
        this.clients = clients;
    }

    public boolean isAuthenticated(String login, String password) {

        return clients.stream()
                .anyMatch(c -> c.getLogin().equals(login) && c.getPassword().equals(password));
    }

    public Client getClientByLoginAndPassword(String login, String password) {

        return clients.stream()
                .filter(c -> c.getLogin().equals(login) && c.getPassword().equals(password))
                .findFirst()
                .orElse(null);
    }
}
