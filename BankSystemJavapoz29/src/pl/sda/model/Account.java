package pl.sda.model;

import pl.sda.exception.InsufficientBalanceException;
import pl.sda.exception.NegativeAmountException;

import java.math.BigDecimal;

public abstract class Account {

    protected String number;

    protected BigDecimal balance;

    protected AccountType type;

    public Account() {
        this.number = "" + System.nanoTime();
        this.balance = BigDecimal.ZERO;
    }

    public Account(String number, BigDecimal balance) {
        this.number = number;
        this.balance = balance;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public AccountType getType() {
        return type;
    }

    public void payment(BigDecimal amount) throws NegativeAmountException {

        if (amount.doubleValue() <= 0) {
            throw new NegativeAmountException("Kwota ujemna bądź zerowa: " + amount);
        }

        balance = balance.add(amount);
    }

    public void withdrawal(BigDecimal amount) throws NegativeAmountException, InsufficientBalanceException {
        if (amount.doubleValue() <= 0) {
            throw new NegativeAmountException("Kwota ujemna bądź zerowa: " + amount);
        }

        if (amount.compareTo(balance) > 0) { //jeśli amount jest większe niż balance
            throw new InsufficientBalanceException("Za mało środków na koncie.");
        }

        balance = balance.subtract(amount);
    }

    public void transfer(Account destination, BigDecimal amount) throws InsufficientBalanceException, NegativeAmountException {

        withdrawal(amount);
        destination.payment(amount);

    }

    @Override
    public String toString() {
        return "Account{" +
                "number='" + number + '\'' +
                ", balance=" + balance +
                ", type=" + type +
                '}';
    }
}
