package pl.sda.model;

import pl.sda.exception.InsufficientBalanceException;
import pl.sda.exception.NegativeAmountException;

import java.math.BigDecimal;

public class CorporateAccount extends Account {

    private double corporateFee = 1.0;

    public CorporateAccount() {
        super();
        type = AccountType.CORPORATE;
    }

    public CorporateAccount(String number, BigDecimal balance) {
        super(number, balance);
        type = AccountType.CORPORATE;
    }

    @Override
    public void withdrawal(BigDecimal amount) throws NegativeAmountException, InsufficientBalanceException {
        BigDecimal resultAmount = amount.add(BigDecimal.valueOf(corporateFee));
        super.withdrawal(resultAmount);
    }

    public double getCorporateFee() {
        return corporateFee;
    }
}
