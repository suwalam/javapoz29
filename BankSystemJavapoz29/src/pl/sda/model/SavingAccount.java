package pl.sda.model;

import pl.sda.exception.NegativeAmountException;

import java.math.BigDecimal;

public class SavingAccount extends Account {

    private double savingFactor = 1.01;

    public SavingAccount() {
        super();
        type = AccountType.SAVING;
    }

    public SavingAccount(String number, BigDecimal balance) {
        super(number, balance);
        type = AccountType.SAVING;
    }

    @Override
    public void payment(BigDecimal amount) throws NegativeAmountException {
        BigDecimal resultAmount = amount.multiply(BigDecimal.valueOf(savingFactor));
        super.payment(resultAmount);
    }

    public double getSavingFactor() {
        return savingFactor;
    }
}
