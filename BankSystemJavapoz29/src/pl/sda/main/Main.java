package pl.sda.main;

import pl.sda.dao.ClientDao;
import pl.sda.dao.ClientDaoFile;
import pl.sda.exception.NegativeAmountException;
import pl.sda.model.Account;
import pl.sda.model.Client;
import pl.sda.model.CurrentAccount;
import pl.sda.model.SavingAccount;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, NegativeAmountException {

        ClientDao clientDao = new ClientDaoFile();

        /*Client client = new Client("Jan", "Kowalski", "12345678910",
                "jan.kowalski", "pass");

        Account current = new CurrentAccount("1343125325235235135", BigDecimal.valueOf(1000.55));
        Account saving = new SavingAccount("43634632456246246", BigDecimal.valueOf(232.32));

        client.getAccounts().add(current);
        client.getAccounts().add(saving);

        List<Client> clients = new ArrayList<>();
        clients.add(client);

        clientDao.saveAll(clients);*/

        List<Client> clientsFromFile = clientDao.getAll();

        for (Client client : clientsFromFile) {
            System.out.println(client);

            for (Account account : client.getAccounts()) {
                System.out.println(account);
            }
        }

        clientsFromFile.get(0).getAccounts().get(1).payment(BigDecimal.valueOf(1000));

        clientDao.saveAll(clientsFromFile);


    }

}
