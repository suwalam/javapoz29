package pl.sda.dao;

import pl.sda.model.*;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ClientDaoFile implements ClientDao {

    public static final String FILE_NAME = "clients.txt";

    public static final String COMMA = ",";

    public static final String CLIENT = "CLIENT";

    @Override
    public void saveAll(List<Client> clients) throws IOException {

        BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME));

        for (Client client : clients) {
            bw.write(getClientRow(client));
            bw.newLine();

            for (Account account : client.getAccounts()) {
                bw.write(getAccountRow(account));
                bw.newLine();
            }
        }

        bw.flush();
        bw.close();

    }

    @Override
    public List<Client> getAll() throws IOException {

        BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));

        List<Client> clients = new ArrayList<>();

        String line;
        Client client = null;

        while ((line = br.readLine()) != null) {

            Account account = null;
            String[] words = line.split(COMMA);

            if (words[0].equals(CLIENT)) {
                client = new Client(words[1], words[2], words[3], words[4], words[5]);
                clients.add(client);
            } else {

                AccountType type = AccountType.valueOf(words[0]);
                BigDecimal balance = BigDecimal.valueOf(Double.parseDouble(words[1]));
                String number = words[2];

                switch (type) {

                    case CURRENT:
                        account = new CurrentAccount(number, balance);
                        break;
                    case SAVING:
                        account = new SavingAccount(number, balance);
                        break;
                    case CORPORATE:
                        account = new CorporateAccount(number, balance);
                        break;
                }

                client.getAccounts().add(account);

            }

        }

        br.close();

        return clients;
    }

    private String getClientRow(Client client) {
        return String.join(COMMA, CLIENT, client.getName(), client.getLastName(),
                client.getPesel(), client.getLogin(), client.getPassword());
    }

    private String getAccountRow(Account account) {
        return String.join(COMMA, account.getType().toString(), account.getBalance().toString(),
                account.getNumber());
    }

}
