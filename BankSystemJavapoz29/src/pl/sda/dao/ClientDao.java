package pl.sda.dao;

import pl.sda.model.Client;

import java.io.IOException;
import java.util.List;

public interface ClientDao {

    void saveAll(List<Client> clients) throws IOException;

    List<Client> getAll() throws IOException;

}
