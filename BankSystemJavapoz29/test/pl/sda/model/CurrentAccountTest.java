package pl.sda.model;


import org.junit.jupiter.api.Test;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.exception.NegativeAmountException;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;

public class CurrentAccountTest {

    @Test
    public void shouldInvokePayment() throws NegativeAmountException {
        //given
        CurrentAccount account = new CurrentAccount();
        BigDecimal testAmount = BigDecimal.valueOf(1000);

        //when
        account.payment(testAmount);

        //then
        Assertions.assertEquals(testAmount, account.getBalance());
    }

    @Test
    public void shouldInvokeWithdrawal() throws NegativeAmountException, InsufficientBalanceException {
        //given
        CurrentAccount account = new CurrentAccount();
        BigDecimal testAmount = BigDecimal.valueOf(1000);
        BigDecimal testAmount2 = BigDecimal.valueOf(500);
        account.payment(testAmount);

        //when
        account.withdrawal(testAmount2);

        //then
        Assertions.assertEquals(testAmount.subtract(testAmount2), account.getBalance());
    }


    @Test
    public void shouldInvokeTransfer() throws NegativeAmountException, InsufficientBalanceException {
        //given
        CurrentAccount accountSrc = new CurrentAccount();
        CurrentAccount accountDst = new CurrentAccount();
        BigDecimal testAmount1 = BigDecimal.valueOf(1000);
        BigDecimal testAmount2 = BigDecimal.valueOf(700);
        accountSrc.payment(testAmount1);

        //when
        accountSrc.transfer(accountDst, testAmount2);

        //then
        Assertions.assertEquals(testAmount1.subtract(testAmount2), accountSrc.getBalance());
        Assertions.assertEquals(testAmount2, accountDst.getBalance());

    }



}