package pl.sda.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.exception.InsufficientBalanceException;
import pl.sda.exception.NegativeAmountException;

import java.math.BigDecimal;

public class CorporateAccountTest {

    @Test
    public void shouldInvokeWithdrawal() throws NegativeAmountException, InsufficientBalanceException {
        //given
        CorporateAccount account = new CorporateAccount();
        BigDecimal testAmount1 = BigDecimal.valueOf(1000);
        BigDecimal testAmount2 = BigDecimal.valueOf(900);

        account.payment(testAmount1);

        //when
        account.withdrawal(testAmount2);

        //then
        BigDecimal expected = testAmount1.subtract(testAmount2).subtract(BigDecimal.valueOf(account.getCorporateFee()));
        Assertions.assertEquals(expected, account.getBalance());

    }
}