package pl.sda.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.exception.NegativeAmountException;

import java.math.BigDecimal;

public class SavingAccountTest {

    @Test
    public void shouldInvokePayment() throws NegativeAmountException {
        //given

        SavingAccount account = new SavingAccount();
        BigDecimal testAmount = BigDecimal.valueOf(1000);

        //when
        account.payment(testAmount);

        //then
        BigDecimal expected = testAmount.multiply(BigDecimal.valueOf(account.getSavingFactor()));
        Assertions.assertEquals(expected, account.getBalance());
    }
}